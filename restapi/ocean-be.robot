*** Settings ***
Resource        ../settings/setLibrary.robot
Resource        ../settings/setVariablesAndKeywords_SM.robot
Test Setup      set headers  ../settings/headers.json
Test Teardown   delete      running/ocean-be

*** Test Cases ***
create 3000 beifs testing
    [Documentation]     L2VPNIF作成を3000回投入
    FOR     ${bundle_id}     IN RANGE    1000    4001
        ${cir_body} =   create beif body   ${pairname}    ${bundle_id}  ${egrt01}   ${egrt02}  ${ifname}
        ${cir_post} =   post    running    ${cir_body}
        log many  ${cir_body}
        log many  ${cir_post['body']}
        should be equal     ${cir_post['status']}   ${201}
        log many  ${cir_post['body']}
    END
