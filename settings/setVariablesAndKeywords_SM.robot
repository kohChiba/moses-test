*** Settings ***
Resource            setValue.robot

*** Keywords ***
create beif body
    [Arguments]     ${per-pair}    ${bundle-id}   ${device-name0}  ${device-name1}  ${interface}
    ${body} =           load json from file     ../json/createCircuit.json
    ${set1} =           update value to json    ${body}     $..'ocean-be:ocean-be'.per-pair   ${per-pair}
    ${set2} =           update value to json    ${set1}     $..'ocean-be:ocean-be'.bundle-id  ${bundle-id}
    ${set3} =           update value to json    ${set2}     $..'ocean-be:ocean-be'.be-intf-members[0].device-name     ${device-name0}
    ${set4} =           update value to json    ${set3}     $..'ocean-be:ocean-be'.be-intf-members[0].device-ref     ${device-name0}
    ${set5} =           update value to json    ${set4}     $..'ocean-be:ocean-be'.be-intf-members[0].interface     ${interface}
    ${set6} =           update value to json    ${set5}     $..'ocean-be:ocean-be'.be-intf-members[1].device-name     ${device-name1}
    ${set7} =           update value to json    ${set6}     $..'ocean-be:ocean-be'.be-intf-members[1].device-ref     ${device-name1}
    ${createBody} =     update value to json    ${set7}     $..'ocean-be:ocean-be'.be-intf-members[1].interface     ${interface}
    [Return]    ${createBody}

delete ocean-be
    ${connection} =  login
    ${read_config} =  get file  ../xml/delete_ocean-be.xml
    edit config  ${connection}  candidate  ${read_config}  merge
    commit  ${connection}

login
    ${login} =      create dictionary   host=${host}     port=830  username=${username}  password=${password}  hostkey_verify=False  look_for_keys=False
    ${connection} =  Connect    &{login}