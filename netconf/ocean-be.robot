*** Settings ***
Resource        ../settings/setLibrary.robot
Resource        ../settings/setVariablesAndKeywords_SM.robot
Test Teardown   delete ocean-be

*** Test Cases ***
Ocean-Be Setting
    ${connection} =  login
    FOR  ${if_description}  IN RANGE  0  3001
        ${convert_if_description_to_str} =  convert to string  ${if_description}
        ${read_config} =  get file  ../xml/ocean-be.xml
        ${replace_if_description} =  replace string using regexp  ${read_config}  TBED  ${convert_if_description_to_str}
        edit config  ${connection}  candidate  ${replace_if_description}  merge
        commit  ${connection}
    END
